from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.sites.models import Site
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from .managers import CheeseUserManager


@python_2_unicode_compatible
class CheeseUser(AbstractUser):
    sites = models.ManyToManyField(Site, blank=True)

    objects = CheeseUserManager()
    all_objects = BaseUserManager()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')


    def __str__(self):
        return self.get_username()

