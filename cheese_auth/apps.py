from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CheeseAuthAppConfig(AppConfig):
    name = 'cheese_auth'
    verbose_name = _("Authentication and Authorization")
