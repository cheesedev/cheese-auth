# coding: utf-8
from __future__ import unicode_literals

from django.conf import settings

from django.db import models
from django.db.utils import IntegrityError

from django.contrib.auth.models import BaseUserManager, Group
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

try:
    from django.apps import apps
    get_model = apps.get_model
except:
    from django.db.models.loading import get_model


class CheeseUserManager(BaseUserManager, CurrentSiteManager):

    def create_user(self, username, email, password=None, **kwargs):
        email = self.normalize_email(email)

        user = self.model(username=username, email=email, **kwargs)
        user.set_password(password)
        try:
            user.save(using=self._db)
        except IntegrityError:
            # add user in Site current
            app, model = settings.AUTH_USER_MODEL.split('.')

            CheeseUser = get_model(app, model)
            user = CheeseUser.all_objects.get(username=username)

            user.set_password(password)
            user.save(using=self._db)

        current_site = Site.objects.get_current()
        user.sites.add(current_site)

        return user

    def create_superuser(self, **kwargs):
        user = self.create_user(**kwargs)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user
