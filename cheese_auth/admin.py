from __future__ import unicode_literals

import copy

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import CheeseUser
from .forms import UserCreationForm, AdminUserChangeForm


USERNAME_FIELD = CheeseUser.USERNAME_FIELD

REQUIRED_FIELDS = (USERNAME_FIELD,) + tuple(CheeseUser.REQUIRED_FIELDS)

BASE_FIELDS = (None, {
    'fields': REQUIRED_FIELDS + ('password',),
})

SIMPLE_PERMISSION_FIELDS = (_(u'Permissions'), {
    'fields': ('is_active', 'is_staff', 'is_superuser',),
})

SITE_FIELDS = (_(u'Sites framework'), {
    'fields': ('sites', )
})

ADVANCED_PERMISSION_FIELDS = copy.deepcopy(SIMPLE_PERMISSION_FIELDS)
ADVANCED_PERMISSION_FIELDS[1]['fields'] += ('groups', 'user_permissions',)

DATE_FIELDS = (_(u'Important dates'), {
    'fields': ('last_login', 'date_joined',),
})


class StrippedUserAdmin(UserAdmin):
    # The forms to add and change user instances
    add_form_template = None
    add_form = UserCreationForm
    form = AdminUserChangeForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('is_active', USERNAME_FIELD, 'is_superuser', 'is_staff',)
    list_display_links = (USERNAME_FIELD,)
    list_filter = ('is_superuser', 'is_staff', 'is_active',)
    fieldsets = (
        BASE_FIELDS,
        SIMPLE_PERMISSION_FIELDS,
        SITE_FIELDS,
    )
    add_fieldsets = (
        (None, {
            'fields': REQUIRED_FIELDS + (
                'password1',
                'password2',
            ),
        }),
    )
    search_fields = (USERNAME_FIELD,)
    ordering = None
    filter_horizontal = tuple()
    readonly_fields = ('last_login', 'date_joined')


class StrippedNamedUserAdmin(StrippedUserAdmin):
    list_display = ('is_active', 'username', 'email', 'first_name',
                    'last_name', 'is_superuser', 'is_staff', )
    list_display_links = ('username', 'email', )
    search_fields = ('username', 'email', 'fist_name', 'last_name', )


class CheeseUserAdmin(StrippedUserAdmin):
    fieldsets = (
        BASE_FIELDS,
        ADVANCED_PERMISSION_FIELDS,
        SITE_FIELDS,
        DATE_FIELDS,
    )
    filter_horizontal = ('groups', 'user_permissions', 'sites')


class NamedUserAdmin(CheeseUserAdmin, StrippedNamedUserAdmin):
    pass

admin.site.register(CheeseUser, NamedUserAdmin)

