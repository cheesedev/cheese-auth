Cheese Auth
----

System authentication working with the framework Sites.

More information Sites framework on [our documentation](https://docs.djangoproject.com/en/1.8/ref/contrib/sites/).


# Quick start

1. Add `cheese_auth` to your INSTALLED_APPS setting like this:

```python
INSTALLED_APPS = (
    # ...
    'cheese_auth',
)
```

2. Set this variable AUTH_USER_MODEL setting like this:

```python
AUTH_USER_MODEL = 'cheese_auth.CheeseUser'
```

2. Run `python manage.py migrate` to create the cheese_auth models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a User (you'll need the Admin app enabled).
